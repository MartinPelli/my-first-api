package com.friedstudios.myfirstapp.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodbyeController {
    @RequestMapping("/goodbye")
    public String Goodbye(){
        return "GoodBye World";
    }
}
